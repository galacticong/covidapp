# HyperTrack

## Screenshot

<div align="center">

<img src="screenshot/Home.png" width="200px"> | <img src="screenshot/About.png" width="200px"> | <img src="screenshot/Search.png" width="200px"> | <img src="screenshot/Detail.png" width="200px">

</div>

## Pages
- OnBoarding
- Index
- Search
- Details
- About

## Reference

[Covid19 API](https://covid19api.com/)