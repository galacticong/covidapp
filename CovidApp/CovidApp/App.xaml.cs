﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CovidApp.Services;
using CovidApp.Views;
using CovidApp.Models;

namespace CovidApp
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<CovidModel>();
            //MainPage = new AppShell();
            MainPage = new OnBoardingPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
