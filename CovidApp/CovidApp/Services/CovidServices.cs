﻿using CovidApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CovidApp.Services
{
    public class CovidServices: ICovidServices
    {
        private const string apiURL = "https://api.covid19api.com/";

        HttpClient _httpClient = new HttpClient();

        public async Task<CovidModel> GetCovidGlobalData()
        {
            try
            {
                string URL = apiURL + $"summary";
                HttpResponseMessage response = await _httpClient.GetAsync(URL);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var json = JsonConvert.DeserializeObject<CovidModel>(content);
                    return json;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<CountryDetailModel>> GetCovidCountryDetailData(string country)
        {
            try
            {
                string URL = apiURL + $"country/{ country }";
                HttpResponseMessage response = await _httpClient.GetAsync(URL);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    List<CountryDetailModel> json = JsonConvert.DeserializeObject<List<CountryDetailModel>>(content);
                    return json;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
