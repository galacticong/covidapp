﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace CovidApp.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class OnBoardingViewModel
    {
        public ObservableCollection<OnBoardingModel> OnBoards { get; set; }

        public int PositionIndex { get; set; }

        public string NextButtonText
        {
            get
            {
                if (PositionIndex == OnBoards.Count - 1)
                    return "LET'S GO";
                return "NEXT";
            }
        }

        public ICommand NextCommand { get; set; }

        public ICommand SkipCommand { get; set; }

        public OnBoardingViewModel()
        {
            OnBoards = new ObservableCollection<OnBoardingModel>
            {
                new OnBoardingModel()
                {
                    Title = "Avoid Close Contact",
                    Description = "Keep your distance from others to protect them from getting sick too.",
                    ImagePath = "board_1.png"
                },
                new OnBoardingModel()
                {
                    Title = "Clean Your Hands",
                    Description = "Wash your hands with soap and water, scrub your hands for at least 20 seconds.",
                    ImagePath = "board_2.png"
                },
                new OnBoardingModel()
                {
                    Title = "Wear a facemask if you are sick",
                    Description = " Consider wearing a face mask when you are sick with a cough or sneezing",
                    ImagePath = "board_3.png"
                },
                new OnBoardingModel()
                {
                    Title = "Stay at home",
                    Description ="Staying at home will help control the spread of the virus to friends, the wider community.",
                    ImagePath ="board_4.png"
                }
            };

            NextCommand = new Command(Next);
            SkipCommand = new Command(StartMainPage);
        }

        public void Next(object obj)
        {
            if (PositionIndex < OnBoards.Count - 1)
            {
                PositionIndex++;
            }
            else
            {
                StartMainPage();
            }
        }

        public void StartMainPage()
        {
            Application.Current.MainPage = new AppShell();
        }

        public class OnBoardingModel
        {
            public string Title { get; set; }
            public string Description { get; set; }
            public string ImagePath { get; set; }
        }
    }
}
