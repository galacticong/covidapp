﻿using CovidApp.Services;
using CovidApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Runtime.CompilerServices;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Dynamic;

namespace CovidApp.ViewModels
{
    public class SearchViewModel : INotifyPropertyChanged
    {
        CovidServices _covidServices = new CovidServices();

        private List<CountryModel> _countryModel { get; set; }
        public List<CountryModel> CountryModel
        {
            get
            {
                return _countryModel;
            }
            set
            {
                _countryModel = value;
                OnPropertyChanged();
            }
        }

        public SearchViewModel()
        {
            GetCountryAsync();
        }

        private async void GetCountryAsync()
        {
            try
            {
                IsBusy = true;
                var jsonData = await _covidServices.GetCovidGlobalData();
                CountryModel = jsonData.Countries;
            }
            finally
            {
                IsBusy = false;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private bool _isBusy { get; set; }
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        public CountryModel CountryLists { get; private set; }
        public ObservableCollection<CountryModel> CountryJSON { get; set; }
    }
}
