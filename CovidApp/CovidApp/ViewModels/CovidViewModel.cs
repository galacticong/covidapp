﻿using CovidApp.Models;
using CovidApp.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CovidApp.ViewModels
{
    public class CovidViewModel: BaseViewModel
    {
        CovidServices _covidServices = new CovidServices();

        private CovidModel _covidModel { get; set; }
        public CovidModel CovidModel
        {
            get { return _covidModel; }
            set
            {
                _covidModel = value;
                OnPropertyChanged();
            }
        }

        public CovidViewModel()
        {
            GetGlobalDataAsync();
        }

        private async void GetGlobalDataAsync()
        { 
            try
            {
                IsBusy = true;
                var jsonData = await _covidServices.GetCovidGlobalData();
                CovidModel = jsonData;
            }
            finally
            {
                IsBusy = false;
            }
        }

    }
}
