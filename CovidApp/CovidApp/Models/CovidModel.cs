﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CovidApp.Models
{
    public class CovidModel
    {
        [JsonProperty("Global")]
        public GlobalModel Global { get; set; }

        public List<CountryModel> Countries { get; set; }
        public DateTime Date { get; set; }
    }

    public class GlobalModel
    {
        [JsonProperty("NewConfirmed")]
        public int NewConfirmed { get; set; }

        [JsonProperty("TotalConfirmed")]
        public int TotalConfirmed { get; set; }

        [JsonProperty("NewDeaths")]
        public int NewDeaths { get; set; }

        [JsonProperty("TotalDeaths")]
        public int TotalDeaths { get; set; }

        [JsonProperty("NewRecovered")]
        public int NewRecovered { get; set; }

        [JsonProperty("TotalRecovered")]
        public int TotalRecovered { get; set; }
    }

    public class Premium
    {
    }

    public class CountryModel
    {
        [JsonProperty("Country")]
        public string Country { get; set; }

        [JsonProperty("CountryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("Slug")]
        public string Slug { get; set; }

        [JsonProperty("NewConfirmed")]
        public int NewConfirmed { get; set; }

        [JsonProperty("TotalConfirmed")]
        public int TotalConfirmed { get; set; }

        [JsonProperty("NewDeaths")]
        public int NewDeaths { get; set; }

        [JsonProperty("TotalDeaths")]
        public int TotalDeaths { get; set; }

        [JsonProperty("NewRecovered")]
        public int NewRecovered { get; set; }

        [JsonProperty("TotalRecovered")]
        public int TotalRecovered { get; set; }
        public DateTime Date { get; set; }
        public Premium Premium { get; set; }
    }

}
