﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CovidApp.Models
{
    public class CountryDetailModel
    {
        [JsonProperty("Country")]
        public string Country { get; set; }

        [JsonProperty("CountryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("Province")]
        public string Province { get; set; }

        [JsonProperty("City")]
        public string City { get; set; }

        [JsonProperty("CityCode")]
        public string CityCode { get; set; }

        [JsonProperty("Lat")]
        public double Lat { get; set; }

        [JsonProperty("Lon")]
        public double Lon { get; set; }

        [JsonProperty("Confirmed")]
        public int Confirmed { get; set; }

        [JsonProperty("Deaths")]
        public int Deaths { get; set; }

        [JsonProperty("Recovered")]
        public int Recovered { get; set; }

        [JsonProperty("Active")]
        public int Active { get; set; }

        [JsonProperty("Date")]
        public DateTime Date { get; set; }
    }
}
